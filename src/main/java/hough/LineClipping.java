package hough;

import java.awt.*;
import java.util.ArrayList;

class LineClipping {

	final byte INSIDE = 0b0000;
	final byte LEFT = 0b0001;
	final byte RIGHT = 0b0010;
	final byte BOTTOM = 0b0100;
	final byte TOP = 0b1000;

	int maxWidth;
	int minWidth;
	int maxHeight;
	int minHeight;

	public LineClipping(int maxWidth, int maxHeight) {
		this.maxHeight = maxHeight;
		this.minHeight = 0;
		this.maxWidth = maxWidth;
		this.minWidth = 0;
	}

	byte computeOutCode(Point point) {
		byte code = INSIDE;

		if (point.getX() < minWidth) code |= LEFT;
		else if (point.getX() > maxWidth) code |= RIGHT;

		if (point.getY() < minHeight) code |= TOP;
		else if (point.getY() > maxHeight) code |= BOTTOM;

		return code;
	}

	ArrayList<Point> clipAndDraw(Point sPoint, Point ePoint) {
		byte outCodeS = computeOutCode(sPoint);
		byte outCodeE = computeOutCode(ePoint);

		boolean accept = false;

		while (true) {
			byte tempOR = (byte) (outCodeS | outCodeE);
			byte tempAND = (byte) (outCodeS & outCodeE);
			if (tempOR == INSIDE) {
				accept = true;
				break;
			} else if (tempAND != INSIDE) {
				break;
			} else {
				double x = 0, y = 0;

				// At least one endpoint is outside the clip rectangle; pick it.
				byte outCodeOut = outCodeS != INSIDE ? outCodeS : outCodeE;

				// Now find the intersection point;
				if ((outCodeOut & TOP) == TOP) {           // point is above the clip rectangle
					x = sPoint.getX() + (ePoint.getX() - sPoint.getX()) * (minHeight - sPoint.getY()) / (ePoint.getY() - sPoint.getY());
					y = minHeight + 1;
				} else if ((outCodeOut & BOTTOM) == BOTTOM) { // point is below the clip rectangle
					x = sPoint.getX() + (ePoint.getX() - sPoint.getX()) * (maxHeight - sPoint.getY()) / (ePoint.getY() - sPoint.getY());
					y = maxHeight - 1;
				} else if ((outCodeOut & RIGHT) == RIGHT) {  // point is to the right of clip rectangle
					y = sPoint.getY() + (ePoint.getY() - sPoint.getY()) * (maxWidth - sPoint.getX()) / (ePoint.getX() - sPoint.getX());
					x = maxWidth - 1;
				} else if ((outCodeOut & LEFT) == LEFT) {   // point is to the left of clip rectangle
					y = sPoint.getY() + (ePoint.getY() - sPoint.getY()) * (minWidth - sPoint.getX()) / (ePoint.getX() - sPoint.getX());
					x = minWidth + 1;
				}

				// Now we move outside point to intersection point to clip
				// and get ready for next pass.
				if (outCodeOut == outCodeS) {
					sPoint.x = (int) Math.round(x);
					sPoint.y = (int) Math.round(y);
					outCodeS = computeOutCode(sPoint);
				} else {
					ePoint.x = (int) Math.round(x);
					ePoint.y = (int) Math.round(y);
					outCodeE = computeOutCode(ePoint);
				}
			}
		}
		if (accept) {
			ArrayList<Point> clippedLinePoints = new ArrayList<>();
			clippedLinePoints.add(0, sPoint);
			clippedLinePoints.add(1, ePoint);
			return clippedLinePoints;
		} else {
			sPoint = new Point(0, 0);
			ePoint = new Point(0, 0);
			ArrayList<Point> clippedLinePoints = new ArrayList<>();
			clippedLinePoints.add(0, sPoint);
			clippedLinePoints.add(1, ePoint);
			return clippedLinePoints;
		}
	}

}
