package hough;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

class ImageUtils {

	public static BufferedImage loadImage(String fileName) {
		BufferedImage colorImage = null;
		try {
			colorImage = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		return colorImage;
	}

	public static void saveImage(BufferedImage img, String fileName) {
		try {
			ImageIO.write(img, "png", new File(fileName));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static BufferedImage byteArrayToBufferedImage(byte[] imageInByte) {
		InputStream in = new ByteArrayInputStream(imageInByte);
		BufferedImage out = null;
		try {
			out = ImageIO.read(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return out;
	}

	public static int[][] bufferedImageToIntArray(BufferedImage bufferedImage) {
		int rgb, r, g, b;
		Color color;
		int width = bufferedImage.getWidth();
		int height = bufferedImage.getHeight();
		int[][] intArray = new int[width][height];
		for (int row = 0; row < height; row++) {
			for (int column = 0; column < width; column++) {
				rgb = bufferedImage.getRGB(column, row);
				color = new Color(rgb, false);
				r = color.getRed();
				g = color.getGreen();
				b = color.getBlue();

				rgb = r + g + b;
				if (rgb > 255) rgb = 255;
				intArray[column][row] = rgb;
			}
		}
		return intArray;
	}

	public static BufferedImage intArrayToBufferedImage(int intArray[][], int width, int height) {
		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int row = 0; row < height; row++) {
			for (int column = 0; column < width; column++) {
				outputImage.setRGB(column, row, intArray[column][row]);
			}
		}
		return outputImage;
	}

	public static BufferedImage intArrayToBufferedImage(int intArray[][][], int width, int height, int radius, int
			minRadius) {
		BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int row = 0; row < height; row++) {
			for (int column = 0; column < width; column++) {
				outputImage.setRGB(column, row, intArray[column][row][radius - minRadius]);
			}
		}
		return outputImage;
	}

}
