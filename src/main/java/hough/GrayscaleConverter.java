package hough;

import java.awt.*;
import java.awt.image.BufferedImage;

class GrayscaleConverter {

	public static BufferedImage convertToGrayscale(BufferedImage colorImage) {
		BufferedImage grayImage = new BufferedImage(colorImage.getWidth(), colorImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
		Graphics g = grayImage.getGraphics();
		g.drawImage(colorImage, 0, 0, null);
		g.dispose();

		return grayImage;
	}
}
