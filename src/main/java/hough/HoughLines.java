package hough;

import java.awt.*;
import java.awt.image.BufferedImage;

class HoughLines {

	private final int minAngle;
	private final int maxAngle;
	private final int angleStep;

	private final BufferedImage inputImage;

	private final int minRadius;
	private final int maxRadius;

	private final double[] sinValues;
	private final double[] cosValues;

	private final double[][] votingTable;

	private final BufferedImage outputImage;

	public HoughLines(BufferedImage inputImage, int angleStep, int minAngle, int maxAngle, int minRadius) {
		this.inputImage = inputImage;
		this.angleStep = angleStep;
		this.minAngle = minAngle;
		this.maxAngle = maxAngle;
		this.minRadius = minRadius;
		this.maxRadius = (int) Math.ceil(Math.hypot(inputImage.getWidth(), inputImage.getHeight()));
		this.outputImage = new BufferedImage(maxAngle, maxRadius, BufferedImage.TYPE_INT_RGB);
		this.sinValues = new double[maxAngle];
		this.cosValues = new double[maxAngle];
		this.votingTable = new double[maxAngle][maxRadius];
		this.populateSinCosValues();
	}

	void populateSinCosValues() {
		for (int a = minAngle; a < maxAngle; a++) {
			sinValues[a] = Math.sin(Math.toRadians(a - 90));
			cosValues[a] = Math.cos(Math.toRadians(a - 90));
		}
	}

	public BufferedImage calculateRadiusAndVote() {
		int rgb, r;
		Color color;
		int halfRadius = maxRadius >>> 1;

		for (int row = 0; row < this.inputImage.getHeight(); row++) {
			for (int column = 0; column < this.inputImage.getWidth(); column++) {
				rgb = this.inputImage.getRGB(column, row);
				color = new Color(rgb, false);
				r = color.getRed();

				if (r == 255) {
					for (int a = minAngle; a < maxAngle; a += angleStep) {
						double radius = (column * cosValues[a]) + (row * sinValues[a]);
						int radiusScaled = (int) Math.round(radius * halfRadius / maxRadius) + halfRadius;
						votingTable[a][radiusScaled] += 1;
						//System.out.println("r: "+radius+" rs: "+radiusScaled);
					}
				}
			}
		}

		int max = 0;
		for (int rng = 0; rng < maxRadius - 1; rng++) {
			for (int angl = 0; angl < maxAngle; angl++) {
				if (votingTable[angl][rng] > max) max = (int) votingTable[angl][rng];
			}
		}

		int value;
		for (int rng = 0; rng < maxRadius - 1; rng++) {
			for (int angl = 0; angl < maxAngle; angl++) {
				value = (int) ((votingTable[angl][rng] / (double) max) * 255.0);
				outputImage.setRGB(angl, rng, 0xff000000 | (value << 16 | value << 8 | value));
			}
		}
		return outputImage;
	}

}



