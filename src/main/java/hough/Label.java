package hough;

class Label {
	public int id;
	public int rank;
	public Label root;

	public Label(int id) {
		this.id = id;
		this.root = this;
		this.rank = 0;
	}

	public Label getRoot() {
		Label thisObj = this;
		Label thisRoot = this.root;

		while (thisObj != thisRoot) {
			thisObj = thisRoot;
			thisRoot = thisRoot.root;
		}
		this.root = thisRoot;
		return this.root;
	}

	public void join(Label root2) {
		//is the rank of root2 less than that of root1?
		if (root2.rank < this.rank) {
			root2.root = this;//yes! then root1 is the parent of root2 (since it has the higher rank)
		} else { //rank of root2 is greater than or equal to that of root1
			this.root = root2;//make root2 the parent
			if (this.rank == root2.rank) {//both ranks are equal?
				root2.rank++;//increment root2, we need to reach a single root for the whole tree
			}
		}
	}
}
