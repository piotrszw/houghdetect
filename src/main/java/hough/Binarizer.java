package hough;

import java.awt.*;
import java.awt.image.BufferedImage;

class Binarizer {

	static BufferedImage binarize(BufferedImage bfimg, int threshold) {

		int rgb, r, g, b;
		Color color;
		int width = bfimg.getWidth();
		int height = bfimg.getHeight();
		threshold = (threshold * 255) / 100;

		BufferedImage binaryImg = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		for (int column = 0; column < width; column++) {
			for (int row = 0; row < height; row++) {
				rgb = bfimg.getRGB(column, row);
				color = new Color(rgb, false);
				r = color.getRed();
				g = color.getGreen();
				b = color.getBlue();

				if (r < threshold && g < threshold && b < threshold) {
					binaryImg.setRGB(column, row, 0x000000);
				} else {
					binaryImg.setRGB(column, row, 0xffffff);
				}
			}
		}
		return binaryImg;
	}
}
