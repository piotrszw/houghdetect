package hough;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

class LineSize {
	int[][] imageArray, validLines;
	int width, height, max, minLength;
	Map<Integer, ArrayList<Point>> coordinates;

	public LineSize(BufferedImage labelledImage) {
		imageArray = ImageUtils.bufferedImageToIntArray(labelledImage);
		width = labelledImage.getWidth();
		height = labelledImage.getHeight();
		max = maximum(imageArray);
		coordinates = new LinkedHashMap<>();
		this.addListToMap(imageArray);
	}

	public LineSize(int[][] labelledArray, int width, int height, int minLength) {
		//imageArray = ImageUtils.bufferedImageToIntArray(labelledArray);
		this.width = width;
		this.height = height;
		max = maximum(labelledArray);
		coordinates = new LinkedHashMap<>();
		this.addListToMap(labelledArray);
		this.minLength = minLength;
		this.validLines = new int[width][height];

	}


	BufferedImage chooseValidLines() {
		BufferedImage validLines = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		//System.out.println("max: " + max);

		for (int i = 1; i <= max; i++) {
			ArrayList<Point> tempPointList;

			tempPointList = coordinates.get(i);
			if (tempPointList == null) continue;
			double maxX = 0D, maxY = 0D, minX = (double) width, minY = (double) height, deltaX, deltaY, elementMaxDimension;

			for (Point p : tempPointList) {
				if (p.getX() > maxX) maxX = p.getX();
				if (p.getX() < minX) minX = p.getX();
				if (p.getY() > maxY) maxY = p.getY();
				if (p.getY() < minY) minY = p.getY();
			}

			deltaX = maxX - minX;
			deltaY = maxY - minY;

			elementMaxDimension = Math.hypot(deltaX, deltaY);

			/*System.out.println("el nr: "+i+" maxX: "+maxX+" maxY: " + maxY + " minX: " + minX+" minY: "+minY + " " +
					"hypot: "+ elementMaxDimension);*/

			if (elementMaxDimension > minLength) {
				for (Point p : tempPointList) {
					validLines.setRGB(p.x, p.y, 0xff000000 | (255 << 16 | 1 << 100 | 1));
				}
			}
		}
		return validLines;
	}

	void addListToMap(int[][] inputArray) {
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				if (inputArray[col][row] != 0) {
					Point tempPoint = new Point();
					tempPoint.x = col;
					tempPoint.y = row;
					addToList(inputArray[col][row], tempPoint);
				}
			}
		}
	}

	public void addToList(Integer mapKey, Point point) {
		ArrayList<Point> points = coordinates.get(mapKey);
		// if list does not exist create it
		if (points == null) {
			points = new ArrayList<>();
			points.add(point);
			coordinates.put(mapKey, points);
		} else {
			// add if item is not already in list
			if (!points.contains(point)) points.add(point);
		}
	}

	int maximum(int[][] inputArray) {
		int max = 0;
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				if (inputArray[col][row] > max) max = inputArray[col][row];
			}
		}
		return max;
	}

}
