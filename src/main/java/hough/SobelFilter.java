package hough;

import java.awt.*;
import java.awt.image.BufferedImage;

class SobelFilter {

	public static BufferedImage sobelFilter(BufferedImage grayImage) {

		BufferedImage sobeled = new BufferedImage(grayImage.getWidth(), grayImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
		int[][] ySobel = {{1, 2, 1}, {0, 0, 0}, {-1, -2, -1}};
		int[][] xSobel = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};

		int xValue, yValue;
		int rgb;
		int r, g, b;
		Color clr;

		int height = grayImage.getHeight();
		int width = grayImage.getWidth();
		int[][] sobelArray = new int[width][height];

		for (int row = 0; row < height; row++) {
			for (int column = 0; column < width; column++) {
				if ((column == 0) || (column == width - 1) || (row == 0) || (row == height - 1)) {
					xValue = 0;
					yValue = 0;
				} else {
					xValue = 0;
					yValue = 0;

					for (int kS = -1; kS <= 1; kS++) {
						for (int iS = -1; iS <= 1; iS++) {
							rgb = grayImage.getRGB(column + iS, row + kS);
							clr = new Color(rgb, false);
							r = clr.getRed();
							g = clr.getGreen();
							b = clr.getBlue();

							xValue = xValue + (r * xSobel[kS + 1][iS + 1] + g * xSobel[kS + 1][iS + 1] + b * xSobel[kS + 1][iS + 1]);
							yValue = yValue + (r * ySobel[kS + 1][iS + 1] + g * ySobel[kS + 1][iS + 1] + b * ySobel[kS + 1][iS + 1]);
						}
					}
				}

				//int totalValue = (int) Math.sqrt((xValue * xValue) + (yValue * yValue));
				int totalValue = Math.abs(xValue) + Math.abs(yValue);
				if (totalValue > 255) totalValue = 255;
				sobelArray[column][row] = totalValue;
				sobeled.setRGB(column, row, 0xff000000 | (totalValue << 16 | totalValue << 8 | totalValue));
			}
		}

		int max = 0;
		for (int row = 0; row < height; row++) {
			for (int column = 0; column < width; column++) {
				if (sobelArray[column][row] > max) max = sobelArray[column][row];
			}
		}

		int value;
		for (int row = 0; row < height; row++) {
			for (int column = 0; column < width; column++) {
				value = (int) (((double) sobelArray[column][row] / (double) max) * 255.0);
				sobeled.setRGB(column, row, 0xff000000 | (value << 16 | value << 8 | value));
			}
		}

		return sobeled;
	}
}
