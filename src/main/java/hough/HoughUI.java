package hough;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

class HoughUI {

	static String filePath;
	static String dir;

	public static void addComponentsToPane(final Container houghPane) {
		houghPane.setLayout(null);

		final JButton openButton = new JButton("Open");
		final JLabel statusbar = new JLabel("Choose image");
		final JLabel elementsLabel = new JLabel("General settings:");
		final JCheckBox linesCheckBox = new JCheckBox("Lines");
		final JCheckBox circlesCheckBox = new JCheckBox("Circles");
		final JCheckBox rectanglesCheckBox = new JCheckBox("Rectangles");
		final JCheckBox binaryImageCheckbox = new JCheckBox("Binary image");
		final JLabel mainThresholdLabel = new JLabel("Edges threshold");
		final JTextField mainThresholdTextField = new JTextField("99");
		final JLabel mainThresholdPercentLabel = new JLabel("%");

		final JSeparator linesSettingsSeparator = new JSeparator(JSeparator.HORIZONTAL);
		final JLabel linesSettingsAreaLabel = new JLabel("LINE SETTINGS");
		final JLabel linesThresholdLabel = new JLabel("Accumulator threshold");
		final JTextField linesThresholdTextField = new JTextField("75");
		final JLabel linesThresholdPercentLabel = new JLabel("%");
		final JLabel linesMinAngleLabel = new JLabel("Min angle");
		final JTextField linesMinAngleTextField = new JTextField("0");
		final JLabel linesMinAngleUnitLabel = new JLabel("°");
		final JLabel linesMaxAngleLabel = new JLabel("Max angle");
		final JTextField linesMaxAngleTextField = new JTextField("180");
		final JLabel linesMaxAngleUnitLabel = new JLabel("°");
		final JLabel linesThicknessLabel = new JLabel("Output lines thickness");
		final JTextField linesThicknessTextField = new JTextField("3");
		final JLabel linesThicknessUnitLabel = new JLabel("px");
		final JLabel linesMinLengthLabel = new JLabel("Minimal line length");
		final JTextField linesMinLengthTextField = new JTextField("80");
		final JLabel linesMinLengthUnitLabel = new JLabel("px");
		final JCheckBox cutLinesCheckBox = new JCheckBox("Trim lines");


		final JSeparator circlesSettingsSeparator = new JSeparator(JSeparator.HORIZONTAL);
		final JLabel circleSettingsAreaLabel = new JLabel("CIRCLE SETTINGS");
		final JLabel circleMinRadiusLabel = new JLabel("Min radius");
		final JTextField circleMinRadiusTextField = new JTextField("10");
		final JLabel circleMaxRadiusLabel = new JLabel("Max radius");
		final JTextField circleMaxRadiusTextField = new JTextField("30");
		final JLabel circleThresholdLabel = new JLabel("Accumulator threshold");
		final JTextField circleThresholdTextField = new JTextField("220");

		final JButton runButton = new JButton("RUN");

		final JSeparator verticalSeparator = new JSeparator(JSeparator.VERTICAL);

		final JLabel mainImageLabel = new JLabel();

		houghPane.add(openButton);
		houghPane.add(statusbar);

		houghPane.add(elementsLabel);
		houghPane.add(linesCheckBox);
		houghPane.add(circlesCheckBox);
		houghPane.add(rectanglesCheckBox);

		houghPane.add(binaryImageCheckbox);
		houghPane.add(mainThresholdLabel);
		houghPane.add(mainThresholdTextField);
		houghPane.add(mainThresholdPercentLabel);

		houghPane.add(linesSettingsSeparator);
		houghPane.add(linesSettingsAreaLabel);
		houghPane.add(linesThresholdLabel);
		houghPane.add(linesThresholdTextField);
		houghPane.add(linesThresholdPercentLabel);
		houghPane.add(linesMinAngleLabel);
		houghPane.add(linesMinAngleTextField);
		houghPane.add(linesMinAngleUnitLabel);
		houghPane.add(linesMaxAngleLabel);
		houghPane.add(linesMaxAngleTextField);
		houghPane.add(linesMaxAngleUnitLabel);
		houghPane.add(linesThicknessLabel);
		houghPane.add(linesThicknessTextField);
		houghPane.add(linesThicknessUnitLabel);
		houghPane.add(linesMinLengthLabel);
		houghPane.add(linesMinLengthTextField);
		houghPane.add(linesMinLengthUnitLabel);
		houghPane.add(cutLinesCheckBox);


		houghPane.add(circlesSettingsSeparator);
		houghPane.add(circleSettingsAreaLabel);
		houghPane.add(circleMinRadiusLabel);
		houghPane.add(circleMinRadiusTextField);
		houghPane.add(circleMaxRadiusLabel);
		houghPane.add(circleMaxRadiusTextField);
		houghPane.add(circleThresholdLabel);
		houghPane.add(circleThresholdTextField);

		houghPane.add(runButton);

		houghPane.add(verticalSeparator);

		houghPane.add(mainImageLabel);

//		houghPane.add();

		Dimension size = runButton.getPreferredSize();

		openButton.setBounds(15, 5, 75, size.height);
		statusbar.setBounds(110, 5, 150, size.height);

		elementsLabel.setBounds(15, 40, 150, size.height);
		linesCheckBox.setBounds(15, 60, 150, size.height);
		linesCheckBox.setSelected(true);
		circlesCheckBox.setBounds(15, 80, 150, size.height);
		rectanglesCheckBox.setBounds(15, 100, 150, size.height);
		rectanglesCheckBox.setEnabled(false);

		binaryImageCheckbox.setBounds(15, 120, 150, size.height);
		binaryImageCheckbox.setEnabled(true);
		mainThresholdLabel.setBounds(15, 150, 100, size.height);
		mainThresholdTextField.setBounds(117, 155, 30, 20);
		mainThresholdPercentLabel.setBounds(147, 155, 10, 20);

		linesSettingsSeparator.setBounds(0, 194, 200, 2);
		linesSettingsAreaLabel.setBounds(15, 195, 150, size.height);

		linesThresholdLabel.setBounds(15, 210, 130, size.height);
		linesThresholdTextField.setBounds(147, 213, 30, 20);
		linesThresholdPercentLabel.setBounds(180, 213, 10, 20);

		linesMinAngleLabel.setBounds(15, 250, 60, size.height);
		linesMinAngleTextField.setBounds(80, 255, 30, 20);
		linesMinAngleUnitLabel.setBounds(112, 250, 10, size.height);

		linesMaxAngleLabel.setBounds(15, 270, 60, size.height);
		linesMaxAngleTextField.setBounds(80, 275, 30, 20);
		linesMaxAngleUnitLabel.setBounds(112, 270, 10, size.height);

		linesThicknessLabel.setBounds(15, 290, 130, size.height);
		linesThicknessTextField.setBounds(150, 295, 20, 20);
		linesThicknessUnitLabel.setBounds(172, 290, 15, size.height);

		linesMinLengthLabel.setBounds(15, 310, 120, size.height);
		linesMinLengthTextField.setBounds(140, 315, 30, 20);
		linesMinLengthUnitLabel.setBounds(172, 310, 15, size.height);
		cutLinesCheckBox.setBounds(15, 335, 150, size.height);
		cutLinesCheckBox.setSelected(true);

		circlesSettingsSeparator.setBounds(0, 400, 200, 2);
		circleSettingsAreaLabel.setBounds(15, 405, 150, size.height);

		circleMinRadiusLabel.setBounds(15, 425, 75, size.height);
		circleMinRadiusTextField.setBounds(90, 430, 30, 20);
		circleMaxRadiusLabel.setBounds(15, 445, 75, size.height);
		circleMaxRadiusTextField.setBounds(90, 450, 30, 20);
		circleThresholdLabel.setBounds(15, 475, 145, size.height);
		circleThresholdTextField.setBounds(147, 478, 30, 20);

		runButton.setBounds(15, 545, 150, size.height);

		verticalSeparator.setBounds(200, 0, 2, 730);

		mainImageLabel.setBounds(205, 15, 700, 700);

		// Create a file chooser that opens up as an Open dialog
		openButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JFileChooser chooser = new JFileChooser();
				chooser.setMultiSelectionEnabled(false);
				//chooser.setCurrentDirectory();
				int option = chooser.showOpenDialog(null);
				if (option == JFileChooser.APPROVE_OPTION) {
					File sf = chooser.getSelectedFile();
					filePath = sf.getPath();
					String filename = sf.getName();
					dir = sf.getParentFile().getPath();
					System.out.println(dir);
					ImageIcon icon = createImageIcon(filePath, filename);
					mainImageLabel.setHorizontalAlignment(JLabel.CENTER);
					mainImageLabel.setVerticalAlignment(JLabel.CENTER);
					mainImageLabel.setIcon(icon);

					statusbar.setText(filename);
				} else {
					statusbar.setText("You canceled.");
				}
			}
		});

		runButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int mainTreshold = Integer.parseInt(mainThresholdTextField.getText());
				int houghLinesTreshold = Integer.parseInt(linesThresholdTextField.getText());
				int houghCirclesTreshold = Integer.parseInt(circleThresholdTextField.getText());
				int minRadius = Integer.parseInt(circleMinRadiusTextField.getText());
				int maxRadius = Integer.parseInt(circleMaxRadiusTextField.getText());
				int minAngle = Integer.parseInt(linesMinAngleTextField.getText());
				int maxAngle = Integer.parseInt(linesMaxAngleTextField.getText());
				float lineThickness = Integer.parseInt(linesThicknessTextField.getText());
				int minLineLength = Integer.parseInt(linesMinLengthTextField.getText());

				boolean findLines = linesCheckBox.isSelected();
				boolean findCircles = circlesCheckBox.isSelected();
				boolean isBinary = binaryImageCheckbox.isSelected();
				boolean cutLines = cutLinesCheckBox.isSelected();


				if (!findCircles && !findLines) return;

				Main.runHough(filePath, dir, mainTreshold, houghLinesTreshold, houghCirclesTreshold, minRadius, maxRadius,
						minAngle, maxAngle, lineThickness, findLines, findCircles, isBinary, cutLines, minLineLength);

				String fname = null;
				if (findLines && findCircles) {
					//mainImageLabel.setIcon(null);
					//System.out.println("LCdir: " + dir);
					fname = "combinedCircles.png";
					//mainImageLabel.setIcon(createImageIcon(dir + "\\combinedCircles.png", "result"));
					//
				} else if (findCircles) {
					//mainImageLabel.setIcon(null);
					fname = "combinedCircles.png";
					System.out.println("Cdir: " + dir);
					//mainImageLabel.setIcon(createImageIcon(dir + "\\combinedCircles.png", "result"));
					//mainImageLabel.repaint();
				} else if (findLines) {
					System.out.println("Ldir: " + dir);
					fname = "combined.png";
					//mainImageLabel.setIcon(createImageIcon(dir + "\\combined.png", "result"));
					//mainImageLabel.repaint();
				}
				ImageIcon icon = new ImageIcon(dir + "\\" + fname, "result");
				icon.getImage().flush();
				mainImageLabel.setIcon(icon);
			}
		});


	}

	// Returns an ImageIcon, or null if the path was invalid.
	private static ImageIcon createImageIcon(String path, String description) {

		if (path != null) {
			return new ImageIcon(path);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}


	public static void createAndShowGUI() {
		//Create and set up the window.
		JFrame frame = new JFrame("Hough Detection");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Set up the content pane.
		addComponentsToPane(frame.getContentPane());

		//Size and display the window.
		frame.setSize(1024, 768);
		frame.setVisible(true);
	}
}





