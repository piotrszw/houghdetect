package hough;

import java.awt.image.BufferedImage;
import java.util.*;

class ElementLabelingEight {
	private final int[][] imageArray;
	private final int[][] labeled;
	private final int height;
	private final int width;
	final Map<Integer, Label> allLabels;

	public ElementLabelingEight(BufferedImage inputImage) {
		this.imageArray = ImageUtils.bufferedImageToIntArray(inputImage);
		this.height = inputImage.getHeight();
		this.width = inputImage.getWidth();
		labeled = new int[width][height];
		allLabels = new LinkedHashMap<>();

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (imageArray[x][y] != 0) imageArray[x][y] = 1;
				labeled[x][y] = 0;
			}
		}
	}


	public ElementLabelingEight(int[][] imageArray, int width, int height) {
		this.imageArray = imageArray;
		this.labeled = new int[width][height];
		this.allLabels = new LinkedHashMap<>();
		this.width = width;
		this.height = height;

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (imageArray[x][y] != 0) imageArray[x][y] = 1;
				labeled[x][y] = 0;
			}
		}
	}


	public ElementLabelingEight(int[][][] houghSpace3d, int width, int height, int radius, int minRadius, int
			threshold) {
		this.imageArray = new int[width][height];
		this.height = height;
		this.width = width;
		this.labeled = new int[width][height];
		this.allLabels = new LinkedHashMap<>();

//		int max = 0;
//		for (int y = 0; y < height; y++) {
//			for (int x = 0; x < width; x++) {
//				if (houghSpace3d[x][y][radius - minRadius] > max) max = houghSpace3d[x][y][radius - minRadius];
//			}
//		}

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (houghSpace3d[x][y][radius - minRadius] >= threshold) imageArray[x][y] = 1;
				labeled[x][y] = 0;
				//System.out.print(imageArray[x][y]);
			}
			//System.out.println();
		}

	}

	public BufferedImage labelElements() {

		int labelP, labelQ, labelR, labelS;
		int nextLabel = 1;

		for (int row = 1; row < height - 1; row++) {
			for (int col = 1; col < width - 1; col++) {

				if (imageArray[col][row] != 0) {
					labelP = labeled[col - 1][row - 1];
					labelQ = labeled[col][row - 1];
					labelR = labeled[col + 1][row + 1];
					labelS = labeled[col - 1][row];

					List<Integer> neighbours = new ArrayList<>();

					if (labelP > 0) neighbours.add(labelP);
					if (labelQ > 0) neighbours.add(labelQ);
					if (labelR > 0) neighbours.add(labelR);
					if (labelS > 0) neighbours.add(labelS);

					int currentLabel;
					if (labelP == 0 && labelQ == 0 && labelR == 0 && labelS == 0) {
						currentLabel = nextLabel;
						allLabels.put(currentLabel, new Label(currentLabel));
						nextLabel++;
					} else {
						currentLabel = Collections.min(neighbours);
						Label root = allLabels.get(currentLabel).getRoot();

						for (Integer n : neighbours) {
							if (root.id != allLabels.get(n).getRoot().id) {
								allLabels.get(n).join(allLabels.get(currentLabel));
							}
						}
					}
					labeled[col][row] = currentLabel;
				}
			}
		}

		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {

				if (labeled[col][row] != 0) {
					labeled[col][row] = allLabels.get(labeled[col][row]).getRoot().id;
				}
			}
		}
		return ImageUtils.intArrayToBufferedImage(labeled, width, height);
	}

	public int[][] labelElementsArray() {

		int labelP, labelQ, labelR, labelS;
		int nextLabel = 1;

		for (int row = 1; row < height - 1; row++) {
			for (int col = 1; col < width - 1; col++) {

				if (imageArray[col][row] != 0) {
					labelP = labeled[col - 1][row - 1];
					labelQ = labeled[col][row - 1];
					labelR = labeled[col + 1][row + 1];
					labelS = labeled[col - 1][row];

					List<Integer> neighbours = new ArrayList<>();

					if (labelP > 0) neighbours.add(labelP);
					if (labelQ > 0) neighbours.add(labelQ);
					if (labelR > 0) neighbours.add(labelR);
					if (labelS > 0) neighbours.add(labelS);

					int currentLabel;
					if (labelP == 0 && labelQ == 0 && labelR == 0 && labelS == 0) {
						currentLabel = nextLabel;
						allLabels.put(currentLabel, new Label(currentLabel));
						nextLabel++;
					} else {
						currentLabel = Collections.min(neighbours);
						Label root = allLabels.get(currentLabel).getRoot();

						for (Integer n : neighbours) {
							if (root.id != allLabels.get(n).getRoot().id) {
								allLabels.get(n).join(allLabels.get(currentLabel));
							}
						}
					}
					labeled[col][row] = currentLabel;
				}
			}
		}

		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {

				if (labeled[col][row] != 0) {
					labeled[col][row] = allLabels.get(labeled[col][row]).getRoot().id;
				}
			}
		}
		return labeled;
	}


}
