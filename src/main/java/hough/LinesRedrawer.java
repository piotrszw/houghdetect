package hough;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

class LinesRedrawer {

	BufferedImage labelledAccumulator, inputImage, combined;
	Centroid centroid;
	ArrayList<Point> centroids;
	String dir;
	float lineThickness;
	int minLength;

	public LinesRedrawer(BufferedImage labelledAccumulator, BufferedImage inputImage, String dir, float
			lineThickness, int minLength) {
		this.labelledAccumulator = labelledAccumulator;
		this.inputImage = inputImage;
		centroid = new Centroid(labelledAccumulator);

		centroids = centroid.calculateCentroids();
		//System.out.println(centroids);
		combined = new BufferedImage(inputImage.getWidth(), inputImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
		this.dir = dir;
		this.lineThickness = lineThickness;
		this.minLength = minLength;
	}

	public void redrawLines(BufferedImage pureInputImage, BufferedImage inputBinaryImage, boolean cutLines) {
		int width = pureInputImage.getWidth(), height = pureInputImage.getHeight();
		//int[][] validLines;

		BufferedImage uncutLines = graphLine(combined, centroids, lineThickness, cutLines);
		try {
			ImageIO.write(uncutLines, "PNG", new File(dir, "unc.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		BufferedImage dupa = ImageUtils.loadImage(dir + "\\unc.png");

		if (cutLines) {
			int[][] labelledArray;
			int rgb1, r1, g1, b1, rgb2, r2, g2, b2;
			Color color1, color2;
			int[][] intArray = new int[width][height];
			for (int row = 0; row < height; row++) {
				for (int column = 0; column < width; column++) {
					rgb1 = uncutLines.getRGB(column, row);
					rgb2 = inputBinaryImage.getRGB(column, row);
					color1 = new Color(rgb1, false);
					color2 = new Color(rgb2, false);
					r1 = color1.getRed();
					g1 = color1.getGreen();
					b1 = color1.getBlue();

					r2 = color2.getRed();
					g2 = color2.getGreen();
					b2 = color2.getBlue();

					if (r1 == 255 && r2 == 255 || g1 == 255 && g2 == 255 || b1 == 255 && b2 == 255) {
						intArray[column][row] = 2 << 16 | g1 << 8 | b1;
					}
				}
			}

			ElementLabelingEight elLab = new ElementLabelingEight(intArray, width, height);
			labelledArray = elLab.labelElementsArray();
			LineSize lineSize = new LineSize(labelledArray, width, height, minLength);
			BufferedImage validLines = lineSize.chooseValidLines();
			ImageUtils.saveImage(validLines, dir + "\\validlines.png");
			//BufferedImage vLines = ImageUtils.intArrayToBufferedImage(validLines, width, height);


			Graphics2D g2d = combined.createGraphics();
			g2d.drawImage(pureInputImage, 0, 0, null);
			g2d.drawImage(validLines, 0, 0, null);
			g2d.dispose();
		} else {
			Graphics2D g2d = combined.createGraphics();
			g2d.drawImage(pureInputImage, 0, 0, null);
			g2d.drawImage(dupa, 0, 0, null);
			g2d.dispose();
		}

		try {
			ImageIO.write(combined, "PNG", new File(dir, "combined.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private BufferedImage graphLine(BufferedImage inputImage, ArrayList<Point> centroids, float lineThickness,
									boolean cutLines) {

		Graphics2D g2d = inputImage.createGraphics();
		LineClipping lc = new LineClipping(inputImage.getWidth(), inputImage.getHeight());
		int maxLength = (int) Math.sqrt(inputImage.getHeight() * inputImage.getHeight() + inputImage.getWidth()
				* inputImage.getWidth());
		Point sPoint = new Point(), ePoint = new Point(), tempPoint = new Point();
		if (cutLines) {
			g2d.setColor(Color.WHITE);
		} else {
			g2d.setColor(Color.RED);
		}

		for (Point p : centroids) {
			tempPoint.x = (int) ((p.getY() * 2 - maxLength) * Math.cos(Math.toRadians(p.getX() - 90)));
			tempPoint.y = (int) ((p.getY() * 2 - maxLength) * Math.sin(Math.toRadians(p.getX() - 90)));

			sPoint.x = (int) (tempPoint.getX() + maxLength * Math.cos(Math.toRadians(p.getX())));
			sPoint.y = (int) (tempPoint.getY() + maxLength * Math.sin(Math.toRadians(p.getX())));
			ePoint.x = (int) (tempPoint.getX() - maxLength * Math.cos(Math.toRadians(p.getX())));
			ePoint.y = (int) (tempPoint.getY() - maxLength * Math.sin(Math.toRadians(p.getX())));

			ArrayList<Point> pts = new ArrayList<>(lc.clipAndDraw(sPoint, ePoint));
			g2d.setStroke(new BasicStroke(lineThickness));
			g2d.drawLine((int) pts.get(0).getX(), (int) pts.get(0).getY(),
					(int) pts.get(1).getX(), (int) pts.get(1).getY()); // Draw a point
		}

		g2d.dispose();
		ImageUtils.saveImage(inputImage, dir + "\\lines.png");
		return inputImage;
	}

}
