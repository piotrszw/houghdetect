package hough;

import java.awt.image.BufferedImage;

class HoughCircles {

	BufferedImage inputImage, sobeledImage;
	int minR, maxR;
	int minAngle, maxAngle;
	int width, height;
	int[][] imageArray;
	//int[][][] houghSpace;
	//int[][][] lut;
	int circleSpaceThreshold;
	String dir;

	public HoughCircles(BufferedImage inputImage, BufferedImage sobeledImage, int minR, int maxR, int
			circleSpaceThreshold, String dir) {
		this.inputImage = inputImage;
		this.sobeledImage = sobeledImage;
		this.width = inputImage.getWidth();
		this.height = inputImage.getHeight();
		this.minR = minR;
		this.maxR = maxR;
		this.minAngle = 0;
		this.maxAngle = 360;
		this.imageArray = ImageUtils.bufferedImageToIntArray(sobeledImage);
		this.circleSpaceThreshold = circleSpaceThreshold;
		this.dir = dir;
	}

	public void run() {
		drawCircles(findCircles(populateLut()));
	}

	private int[][][] populateLut() {
		int[][][] lut = new int[2][maxAngle][maxR];
		int rCos, rSin;
		for (int radius = minR; radius < maxR; radius++) {
			for (int angl = minAngle; angl < maxAngle; angl++) {
				rCos = (int) Math.round(radius * Math.cos(Math.toRadians(angl)));
				rSin = (int) Math.round(radius * Math.sin(Math.toRadians(angl)));

				lut[0][angl][radius] = rCos;
				lut[1][angl][radius] = rSin;
			}
		}
		return lut;
	}

	private int[][][] findCircles(int[][][] lut) {
		int a, b;
		int[][][] houghSpace = new int[width][height][(maxR - minR) + 1];


		for (int radius = minR; radius < maxR; radius++) {
			for (int angl = minAngle; angl < maxAngle; angl++) {
				for (int row = 0; row < height; row++) {
					for (int column = 0; column < width; column++) {
						if (imageArray[column][row] != 0) {
							//System.out.println(column + " "+row+" "+imageArray[column][row]);
							a = column + lut[1][angl][radius];
							b = row + lut[0][angl][radius];
							if ((b >= 0) & (b < height) & (a >= 0) & (a < width)) {
								//System.out.println("a: " + a + " b; " + b + " calcR: " + (radius - minR));
								houghSpace[a][b][(radius - minR)] += 1;
								//System.out.println(houghSpace[a][b][(radius - minR)]);
							}
						}
					}
				}
			}
		}
		return houghSpace;
	}

	private void drawCircles(int[][][] houghSpace) {
		for (int radius = minR; radius < maxR; radius++) {
			ElementLabelingEight cL = new ElementLabelingEight(houghSpace, width, height, radius, minR, circleSpaceThreshold);
			CircleRedrawer cr = new CircleRedrawer(cL.labelElementsArray(), sobeledImage, radius, dir);
			cr.redrawCircles(inputImage);
		}
	}

}
