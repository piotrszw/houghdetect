package hough;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

class Centroid {
	int[][] imageArray;
	int width, height, max;
	Map<Integer, ArrayList<Point>> coordinates;

	public Centroid(BufferedImage labelledImage) {
		imageArray = ImageUtils.bufferedImageToIntArray(labelledImage);
		width = labelledImage.getWidth();
		height = labelledImage.getHeight();
		max = maximum(imageArray);
		coordinates = new LinkedHashMap<>();
		this.addListToMap(imageArray);
	}

	public Centroid(int[][] labelledArray, int width, int height) {
		//imageArray = ImageUtils.bufferedImageToIntArray(labelledArray);
		this.width = width;
		this.height = height;
		max = maximum(labelledArray);
		coordinates = new LinkedHashMap<>();
		this.addListToMap(labelledArray);
	}

	ArrayList<Point> calculateCentroids() {

		ArrayList<Point> centroids = new ArrayList<>();
		//System.out.println("max: " + max);

		for (int i = 1; i <= max; i++) {
			ArrayList<Point> tempPointList;
			int summedX = 0, summedY = 0;

			tempPointList = coordinates.get(i);
			if (tempPointList == null) continue;

			for (Point p : tempPointList) {
				summedX += p.getX();
				summedY += p.getY();
			}
			centroids.add(new Point(summedX / tempPointList.size(), summedY / tempPointList.size()));
		}
		return centroids;
	}

	void addListToMap(int[][] inputArray) {
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				if (inputArray[col][row] != 0) {
					Point tempPoint = new Point();
					tempPoint.x = col;
					tempPoint.y = row;
					addToList(inputArray[col][row], tempPoint);
				}
			}
		}
	}

	public void addToList(Integer mapKey, Point point) {
		ArrayList<Point> points = coordinates.get(mapKey);
		// if list does not exist create it
		if (points == null) {
			points = new ArrayList<>();
			points.add(point);
			coordinates.put(mapKey, points);
		} else {
			// add if item is not already in list
			if (!points.contains(point)) points.add(point);
		}
	}

	int maximum(int[][] inputArray) {
		int max = 0;
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				if (inputArray[col][row] > max) max = inputArray[col][row];
			}
		}
		return max;
	}

}
