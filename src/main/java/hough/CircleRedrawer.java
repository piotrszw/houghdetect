package hough;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

class CircleRedrawer {

	BufferedImage labelledAccumulator, inputImage, combinedCircles;
	Centroid centroid;
	ArrayList<Point> centroids;
	int width, height;
	int radius;
	String dir;

	public CircleRedrawer(BufferedImage labelledAccumulator, BufferedImage inputImage, int minRadius, int maxRadius) {
		this.labelledAccumulator = labelledAccumulator;
		this.inputImage = inputImage;
		centroid = new Centroid(labelledAccumulator);
		this.width = inputImage.getWidth();
		this.height = inputImage.getHeight();
		centroids = centroid.calculateCentroids();
		//System.out.println(centroids);
		combinedCircles = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	}

	public CircleRedrawer(int[][] labelledArray, BufferedImage inputImage, int radius, String dir) {

		this.inputImage = inputImage;
		this.width = inputImage.getWidth();
		this.height = inputImage.getHeight();
		this.radius = radius;
		centroid = new Centroid(labelledArray, width, height);
		centroids = centroid.calculateCentroids();
		//System.out.println("radius: "+radius+" centroids: "+centroids);
		combinedCircles = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		this.dir = dir;
	}

	private BufferedImage graphCircles(BufferedImage inputImage, ArrayList<Point> centroids) {
		Graphics g = inputImage.getGraphics();
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		int diameter = 2 * radius;

		for (Point p : centroids) {
			g2d.setColor(new Color(100, 100, 255));
			g2d.drawOval(p.x - radius, p.y - radius, diameter, diameter);
			g2d.setColor(Color.YELLOW);
			g2d.drawLine(p.x - 2, p.y, p.x + 2, p.y);
			g2d.drawLine(p.x, p.y - 2, p.x, p.y + 2);
		}

		g2d.dispose();
		//ImageUtils.saveImage(inputImage,dir+"\\kolka.png");
		return inputImage;
	}

	public void redrawCircles(BufferedImage pureInputImage) {
		BufferedImage tmp = graphCircles(pureInputImage, centroids);
		Graphics g = combinedCircles.getGraphics();
		g.drawImage(pureInputImage, 0, 0, null);
		g.drawImage(tmp, 0, 0, null);
		g.dispose();

		try {
			ImageIO.write(combinedCircles, "PNG", new File(dir, "combinedCircles.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
