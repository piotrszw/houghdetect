package hough;

import java.awt.image.BufferedImage;

class Main {

	public static void main(String args[]) {

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				HoughUI.createAndShowGUI();
			}
		});
	}

	public static void runHough(String filePath, String dir, int mainTreshold, int houghLinesTreshold,
								int houghCirclesTreshold, int minRadius, int maxRadius, int minAngle, int maxAngle,
								float lineThickness, boolean findLines,
								boolean findCircles, boolean isBinary, boolean cutLines, int minLineLength) {
		BufferedImage img = ImageUtils.loadImage(filePath);
		BufferedImage grayimage = GrayscaleConverter.convertToGrayscale(img);
		BufferedImage sobimg = SobelFilter.sobelFilter(grayimage);
		ImageUtils.saveImage(sobimg, dir + "\\sobOutput.png");
		BufferedImage bImg = Binarizer.binarize(sobimg, mainTreshold);
		ImageUtils.saveImage(bImg, dir + "\\binOutput.png");

		if (findLines && !findCircles) {
			HoughLines hl;
			if (isBinary) {
				hl = new HoughLines(img, 1, minAngle, maxAngle, 0);
			} else {
				hl = new HoughLines(bImg, 1, minAngle, maxAngle, 0);
			}
			BufferedImage voteOut = hl.calculateRadiusAndVote();
			ImageUtils.saveImage(voteOut, dir + "\\voteOut.png");
			BufferedImage bVoteOut = Binarizer.binarize(voteOut, houghLinesTreshold);
			ImageUtils.saveImage(bVoteOut, dir + "\\bVoteOut.png");
			ElementLabelingEight elL = new ElementLabelingEight(bVoteOut);
			BufferedImage elLOut = elL.labelElements();
			ImageUtils.saveImage(elLOut, dir + "\\elLOut.png");
			LinesRedrawer lD = new LinesRedrawer(elLOut, img, dir, lineThickness, minLineLength);
			lD.redrawLines(img, bImg, cutLines);
		}

		if (findCircles && !findLines) {

			HoughCircles houghCircles;
			if (isBinary) {
				houghCircles = new HoughCircles(img, img, minRadius, maxRadius, houghCirclesTreshold, dir);
			} else {
				houghCircles = new HoughCircles(img, bImg, minRadius, maxRadius, houghCirclesTreshold, dir);
			}
			houghCircles.run();
		}

		if (findLines && findCircles) {
			HoughLines hl;
			if (isBinary) {
				hl = new HoughLines(img, 1, minAngle, maxAngle, 0);
			} else {
				hl = new HoughLines(bImg, 1, minAngle, maxAngle, 0);
			}
			BufferedImage voteOut = hl.calculateRadiusAndVote();
			ImageUtils.saveImage(voteOut, dir + "\\voteOut.png");
			BufferedImage bVoteOut = Binarizer.binarize(voteOut, houghLinesTreshold);
			ImageUtils.saveImage(bVoteOut, dir + "\\bVoteOut.png");
			ElementLabelingEight elL = new ElementLabelingEight(bVoteOut);
			BufferedImage elLOut = elL.labelElements();
			ImageUtils.saveImage(elLOut, dir + "\\elLOut.png");
			LinesRedrawer lD = new LinesRedrawer(elLOut, img, dir, lineThickness, minLineLength);
			lD.redrawLines(img, bImg, cutLines);
			BufferedImage lines = ImageUtils.loadImage(dir + "\\combined.png");
			HoughCircles houghCircles;
			if (isBinary) {
				houghCircles = new HoughCircles(lines, img, minRadius, maxRadius, houghCirclesTreshold, dir);
			} else {
				houghCircles = new HoughCircles(lines, bImg, minRadius, maxRadius, houghCirclesTreshold, dir);
			}
			houghCircles.run();
		}

	}
}